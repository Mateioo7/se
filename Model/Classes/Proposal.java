package Classes;

import java.util.*;

public class Proposal {

    public Proposal() {
    }

    private Long id;

    private String name;

    private String abstract;

    private String paper;

    private Set<String> topics;

    private Set<String> keywords;

    private Set<String> authors;

}