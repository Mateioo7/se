package model;

public class ProposalReview extends Entity<Long> {
    private Proposal proposal;
    private User reviewer;
    private ReviewQualifier qualifier;
    private String recommendation;

    public Proposal getProposal() {
        return proposal;
    }

    public User getReviewer() {
        return reviewer;
    }

    public ReviewQualifier getQualifier() {
        return qualifier;
    }

    public String getRecommendation() {
        return recommendation;
    }

    public void setProposal(Proposal proposal) {
        this.proposal = proposal;
    }

    public void setReviewer(User reviewer) {
        this.reviewer = reviewer;
    }

    public void setQualifier(ReviewQualifier qualifier) {
        this.qualifier = qualifier;
    }

    public void setRecommendation(String recommendation) {
        this.recommendation = recommendation;
    }
}