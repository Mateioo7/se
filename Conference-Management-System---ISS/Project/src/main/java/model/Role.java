package model;

public enum Role {
    ADMIN,
    AUTHOR,
    SPEAKER,
    PC_MEMBER,
    LISTENER;
}