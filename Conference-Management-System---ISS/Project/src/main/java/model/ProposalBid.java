package model;

public class ProposalBid extends Entity<Long> {
    private Proposal proposal;
    private User reviewer;
    private BidQualifier qualifier;

    public Proposal getProposal() {
        return proposal;
    }

    public User getReviewer() {
        return reviewer;
    }

    public BidQualifier getQualifier() {
        return qualifier;
    }

    public void setProposal(Proposal proposal) {
        this.proposal = proposal;
    }

    public void setReviewer(User reviewer) {
        this.reviewer = reviewer;
    }

    public void setQualifier(BidQualifier qualifier) {
        this.qualifier = qualifier;
    }
}