package model;

import java.util.HashSet;
import java.util.Set;

public class ConferenceSection extends Entity<Long> {
    private String name;
    private Room room;
    private User chair;
    private Set<Proposal> proposals = new HashSet<>();

    public String getName() {
        return name;
    }

    public Room getRoom() {
        return room;
    }

    public User getChair() {
        return chair;
    }

    public Set<Proposal> getProposals() {
        return proposals;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public void setChair(User chair) {
        this.chair = chair;
    }
}