package model;

import java.util.HashSet;
import java.util.Set;

public class Conference extends Entity<Long> {
    private String name;
    private Set<ConferenceSection> sections = new HashSet<>();
    private Schedule proposalSchedule;
    private Schedule bidSchedule;
    private Schedule reviewSchedule;
    private Schedule conflictSchedule;
    private int numberOfReviewers;

    public String getName() {
        return name;
    }

    public Set<ConferenceSection> getSections() {
        return sections;
    }

    public Schedule getProposalSchedule() {
        return proposalSchedule;
    }

    public Schedule getBidSchedule() {
        return bidSchedule;
    }

    public Schedule getReviewSchedule() {
        return reviewSchedule;
    }

    public Schedule getConflictSchedule() {
        return conflictSchedule;
    }

    public int getNumberOfReviewers() {
        return numberOfReviewers;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setProposalSchedule(Schedule proposalSchedule) {
        this.proposalSchedule = proposalSchedule;
    }

    public void setBidSchedule(Schedule bidSchedule) {
        this.bidSchedule = bidSchedule;
    }

    public void setReviewSchedule(Schedule reviewSchedule) {
        this.reviewSchedule = reviewSchedule;
    }

    public void setConflictSchedule(Schedule conflictSchedule) {
        this.conflictSchedule = conflictSchedule;
    }

    public void setNumberOfReviewers(int numberOfReviewers) {
        this.numberOfReviewers = numberOfReviewers;
    }
}