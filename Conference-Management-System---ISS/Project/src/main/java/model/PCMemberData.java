package model;

public class PCMemberData extends Entity<Long> {
    private String webPage;
    private User authorUser;

    public String getWebPage() {
        return webPage;
    }

    public User getAuthorUser() {
        return authorUser;
    }

    public void setWebPage(String webPage) {
        this.webPage = webPage;
    }

    public void setAuthorUser(User authorUser) {
        this.authorUser = authorUser;
    }
}