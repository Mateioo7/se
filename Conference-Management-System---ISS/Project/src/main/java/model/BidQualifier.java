package model;

public enum BidQualifier {
    REJECT,
    BORDERLINE,
    ACCEPT;
}