package model;

import java.util.HashSet;
import java.util.Set;

public class Proposal extends Entity<Long> {
    private User submitter;
    private String name;
    private String summary;
    private String paperPath;
    private Set<String> topics = new HashSet<>();
    private Set<String> keywords = new HashSet<>();
    private Set<User> authors = new HashSet<>();
    private String presentationPath;

    public User getSubmitter() {
        return submitter;
    }

    public String getName() {
        return name;
    }

    public String getSummary() {
        return summary;
    }

    public String getPaperPath() {
        return paperPath;
    }

    public Set<String> getTopics() {
        return topics;
    }

    public Set<String> getKeywords() {
        return keywords;
    }

    public Set<User> getAuthors() {
        return authors;
    }

    public String getPresentationPath() {
        return presentationPath;
    }

    public void setSubmitter(User submitter) {
        this.submitter = submitter;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public void setPaperPath(String paperPath) {
        this.paperPath = paperPath;
    }

    public void setPresentationPath(String presentationPath) {
        this.presentationPath = presentationPath;
    }
}