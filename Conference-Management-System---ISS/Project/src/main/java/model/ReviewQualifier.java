package model;

public enum ReviewQualifier {
    STRONG_REJECT,
    REJECT,
    WEAK_REJECT,
    BORDERLINE,
    WEAK_ACCEPT,
    ACCEPT,
    STRONG_ACCEPT;
}